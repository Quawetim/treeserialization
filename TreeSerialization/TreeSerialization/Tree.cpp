#include "Tree.h"

Tree::Tree() : _root(NULL) {};

Tree::~Tree() {};

///<summary>����� ������ ������.</summary>
///<param name='root'>������ ������.</root>
void Tree::setRoot(NodeOfBase* root)
{
    _root = root;
}

///<summary>���������� ������ ������.</summary>
NodeOfBase* Tree::getRoot()
{
    return _root;
}

///<summary>����� ������ � �������. (root -> list -> list -> ...)</summary>
///<param name='node'>����, � �������� ���������� �����.</param>
///<param name='log'>���-����.</param>
bool Tree::DFS(NodeOfBase* node, std::ofstream &log)
{
    if (node == NULL)
    {
        std::cout << "<Error><Tree::DFS> Node is NULL." << std::endl <<std::endl;
        log << "<Error><Tree::DFS> Node is NULL." << std::endl << std::endl;
        return false;
    }

    std::cout << node->getDataAsString() << std::endl;

    for (size_t i = 0; i < node->ListOfChildrens.size(); i++)
    {
        DFS(node->ListOfChildrens[i], log);
    }

    return true;
}

///<summary>����� ������ � ������ � ����� �������� �����.</summary>
///<param name='node'>����, � �������� ���������� �����.</param>
///<param name='log'>���-����.</param>
///<returns>���������� ���������� ������.</returns>
bool Tree::BFS(NodeOfBase* node, std::ofstream &log)
{
    if (node == NULL)
    {
        std::cout << "<Error><Tree::BFS> Start node is NULL." << std::endl << std::endl;
        log << "<Error><Tree::BFS> Start node is NULL." << std::endl << std::endl;
        return false;
    }

    std::cout << "Printing tree to screen. Format: Type ParentNode_data (nil | ChildNode1_data, ChildNode2_data, ...)" << std::endl << std::endl;

    std::queue<NodeOfBase*> q;

    q.push(node);

    NodeOfBase* prev_node = NULL;

    while (!q.empty())
    {
        NodeOfBase *curr_node = q.front();
        q.pop();

        if (curr_node == prev_node)
        {
            std::cout << "<Error><Tree::BFS> Loop detected. Parent = child." << std::endl << std::endl;
            log << "<Error><Tree::BFS> Loop detected. Parent = child." << std::endl << std::endl;
            return false;
        }

        std::cout.width(5);
        std::cout << (char)curr_node->getNodeType() << "\t";
        std::cout.width(10);
        std::cout << curr_node->getDataAsString() << "\t(";

        size_t list_size = curr_node->ListOfChildrens.size();

        if (list_size == 0)
        {
            std::cout << "nil";
        }
        else
        {
            for (size_t i = 0; i < list_size; i++)
            {
                if (curr_node->ListOfChildrens[i] != NULL)
                {
                    q.push(curr_node->ListOfChildrens[i]);
                   
                    if (i != curr_node->ListOfChildrens.size() - 1)
                    {
                        std::cout << curr_node->ListOfChildrens[i]->getDataAsString() << ", ";
                    }
                    else
                    {
                        std::cout << curr_node->ListOfChildrens[i]->getDataAsString();
                    }
                }
                else
                {
                    std::cout << std::endl << "<Error><Tree::BFS> For node " << curr_node->getNodeNumber() << " no childs found. Should be " << list_size << ". Wrog file format?" << std::endl << std::endl;
                    log << std::endl << "<Error><Tree::BFS> For node " << curr_node->getNodeNumber() << " no childs found. Should be " << list_size << ". Wrog file format?" << std::endl << std::endl;
                    return false;
                }
            }
        }

        std::cout << ")";

        std::cout << std::endl;

        prev_node = curr_node;
    }

    std::cout << std::endl;

    return true;
}

///<summary>����� ������ �� �����.</summary>
///<param name='log'>���-����.</param>
///<returns>���������� ������ �� �����.</returns>
bool Tree::PrintToScreen(std::ofstream &log)
{
    if (_debug)
    {
        std::cout << "<Info><Tree::PrintToScreen> Printing tree." << std::endl;
    }

    log << "<Info><Tree::PrintToScreen> Printing tree." << std::endl;

    if (!BFS(_root, log))
    {
        std::cout << "<Error><Tree::PrintToScreen> Can't print tree." << std::endl << std::endl;
        log << "<Error><Tree::PrintToScreen> Can't print tree." << std::endl << std::endl;
        return false;
    }

    if (_debug)
    {
        std::cout << "<Info><Tree::PrintToScreen> Complete succesfully." << std::endl << std::endl;
    }

    log << "<Info><Tree::PrintToScreen> Complete succesfully." << std::endl << std::endl;

    return true;
}