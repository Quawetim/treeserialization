#include <iostream>
#include <fstream>
#include <string>
#include <vector>

enum NodeType { n_int = 'i', n_float = 'f', n_string = 's' };

extern bool _debug;

//-----------------------NodeOfBase------------------------//
///<summary>��������� "���� ������". ������ ��������� �� ��������.</summary>
class NodeOfBase
{
protected:
    int _number;

    ///<summary>����� ����� ���� ������.</summary>
    ///<param name='value'>����� ����.</param>
    void setNodeNumber(int value);      
public:
    std::vector<NodeOfBase*> ListOfChildrens;

    virtual ~NodeOfBase() {};
    virtual NodeType getNodeType() = 0;
    virtual void getData(void* data) = 0;
    virtual std::string getDataAsString() = 0;   

    ///<summary>�������� ����� ���� ������.</summary>
    ///<returns>����� ����.</returns>
    int getNodeNumber(); 
};

//-----------------------NodeOfInt------------------------//
///<summary>��������� "���� ������", �������� ������� ���� (int).</summary>
class NodeOfInt : public NodeOfBase
{
private:
    int _data;

public:
    ///<summary>�����������.</summary>
    ///<param name='number'>����� ����.</param>
    ///<param name='value'>��������, �������� �����.</param>
    ///<param name='log'>���-����.</param>
    NodeOfInt(int number, int value, std::ofstream &log);
    ~NodeOfInt();

    ///<summary>���������� �������� �������� (int).</summary>
    void getData(void* data);

    ///<summary>���������� �������� �������� (int) � ���� ������.</summary>
    std::string getDataAsString();

    ///<summary>���������� ��� ���� (n_int).</summary>
    NodeType getNodeType();
};

//-----------------------NodeOfFloat------------------------//
///<summary>��������� "���� ������", �������� ������� ���� (float).</summary>
class NodeOfFloat : public NodeOfBase
{
private:
    float _data;

public:
    ///<summary>�����������.</summary>
    ///<param name='number'>����� ����.</param>
    ///<param name='value'>��������, �������� �����.</param>
    ///<param name='log'>���-����.</param>
    NodeOfFloat(int number, float value, std::ofstream &log);
    ~NodeOfFloat();

    ///<summary>���������� �������� �������� (float).</summary>
    void getData(void* data);

    ///<summary>���������� �������� �������� (float) � ���� ������.</summary>
    std::string getDataAsString();

    ///<summary>���������� ��� ���� (n_float).</summary>
    NodeType getNodeType();
};

//-----------------------NodeOfString------------------------//
///<summary>��������� "���� ������", �������� ������� ���� (string).</summary>
class NodeOfString : public NodeOfBase
{
private:
    std::string _data;

public:
    ///<summary>�����������.</summary>
    ///<param name='number'>����� ����.</param>
    ///<param name='value'>��������, �������� �����.</param>
    ///<param name='log'>���-����.</param>
    NodeOfString(int number, std::string value, std::ofstream &log);
    ~NodeOfString();

    ///<summary>���������� �������� �������� (string).</summary>
    void getData(void* data);

    ///<summary>���������� �������� ������.</summary>
    std::string getDataAsString();

    ///<summary>���������� ��� ���� (n_string).</summary>
    NodeType getNodeType();
};