#include "Tree.h"

class Serializator
{
private:
    NodeOfBase* FindNodeByNumber(int number, std::vector<NodeOfBase*> nodes, std::ofstream &log);

public:
    Serializator();
    ~Serializator();

    ///<summary>��������� ������������ ������ (����� � ����).
    ///<para>������ �����:</para>
    ///<para>type	data	number_of_childs	[numbers]</para>
    ///<para>type - ��� �������� �����: i - �����, f - ������������, s - ������;</para>
    ///<para>data - �������� ��������;</para>
    ///<para>number_of_childs - ����� ����� (��������) ��� 0, ���� �� ���.</para>
    ///<para>[numbers] - ������ �����, ������� �������� ������.</para>
    ///</summary>
    ///<param name='tree'>��������� �� ������.</param>
    ///<param name='filepath'>����, � ������� ����� �������� ��������������� ������.</param>
    ///<param name='log'>���-����.</param>
    ///<returns>���������� ���������� ������������.</returns>
    bool Serialize(Tree *tree, std::string filepath, std::ofstream &log);

    ///<summary>��������� �������������� ������ (������ �� �����).
    ///<para>������ �����:</para>
    ///<para>type	data	number_of_childs	[numbers]</para>
    ///<para>type - ��� �������� �����: i - �����, f - ������������, s - ������;</para>
    ///<para>data - �������� ��������;</para>
    ///<para>number_of_childs - ����� ����� (��������) ��� 0, ���� �� ���.</para>
    ///<para>[numbers] - ������ �����, ������� �������� ������.</para>
    ///</summary>
    ///<param name='tree'>����� ����� ������ ����� ��������������.</param>
    ///<param name='filepath'>����, � ������� ��������� ����������������� ������.</param>
    ///<param name='log'>���-����.</param>
    ///<returns>���������� ���������� ��������������, ���� ������.</returns>
    bool Deserialize(Tree *tree, std::string filepath, std::ofstream &log);
};
