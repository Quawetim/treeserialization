#include "NodeOf.h"

bool _debug = false;

//-----------------------NodeOfBase------------------------//
///<summary>����� ����� ���� ������.</summary>
///<param name='value'>����� ����.</param>
void NodeOfBase::setNodeNumber(int value)
{
    _number = value;
}

///<summary>�������� ����� ���� ������.</summary>
///<returns>����� ����.</returns>
int NodeOfBase::getNodeNumber()
{
    return _number;
}

//-----------------------NodeOfInt------------------------//
///<summary>�����������.</summary>
///<param name='number'>����� ����.</param>
///<param name='value'>��������, �������� �����.</param>
///<param name='log'>���-����.</param>
NodeOfInt::NodeOfInt(int number, int value, std::ofstream &log) : _data(value)
{
    _number = number;

    if (_debug)
    {
        std::cout << "<Info><NodeOfInt::NodeOfInt> Creating node with data: " << _data << std::endl;
    }

    log << "<Info><NodeOfInt::NodeOfInt> Creating node with data: " << _data << std::endl;
}

NodeOfInt::~NodeOfInt() {};

///<summary>���������� �������� �������� (int).</summary>
void NodeOfInt::getData(void* data)
{
    *static_cast<int*>(data) = _data;
}

///<summary>���������� �������� �������� (int) � ���� ������.</summary>
std::string NodeOfInt::getDataAsString()
{
    return std::to_string(_data);
}

///<summary>���������� ��� ���� (n_int).</summary>
NodeType NodeOfInt::getNodeType()
{
    return n_int;
}

//-----------------------NodeOfFloat------------------------//
///<summary>�����������.</summary>
///<param name='number'>����� ����.</param>
///<param name='value'>��������, �������� �����.</param>
///<param name='log'>���-����.</param>
NodeOfFloat::NodeOfFloat(int number, float value, std::ofstream &log) : _data(value)
{
    _number = number;

    if (_debug)
    {
        std::cout << "<Info><NodeOfFloat::NodeOfFloat> Creating node with data: " << _data << std::endl;
    }

    log << "<Info><NodeOfFloat::NodeOfFloat> Creating node with data: " << _data << std::endl;
}

NodeOfFloat::~NodeOfFloat() {};

///<summary>���������� �������� �������� (float).</summary>
void NodeOfFloat::getData(void* data)
{
    *static_cast<float*>(data) = _data;
}

///<summary>���������� �������� �������� (float) � ���� ������.</summary>
std::string NodeOfFloat::getDataAsString()
{   
    return std::to_string(_data);
}

///<summary>���������� ��� ���� (n_float).</summary>
NodeType NodeOfFloat::getNodeType()
{
    return n_float;
}

//-----------------------NodeOfString------------------------//
///<summary>�����������.</summary>
///<param name='number'>����� ����.</param>
///<param name='value'>��������, �������� �����.</param>
///<param name='log'>���-����.</param>
NodeOfString::NodeOfString(int number, std::string value, std::ofstream &log) : _data(value)
{
    _number = number;

    if (_debug)
    {
        std::cout << "<Info><NodeOfString::NodeOfString> Creating node with data: " << _data << std::endl;
    }

    log << "<Info><NodeOfString::NodeOfString> Creating node with data: " << _data << std::endl;
}

NodeOfString::~NodeOfString() {};

///<summary>���������� �������� �������� (string).</summary>
void NodeOfString::getData(void* data)
{
    *static_cast<std::string*>(data) = _data;
}

///<summary>���������� �������� ������.</summary>
std::string NodeOfString::getDataAsString()
{
    return _data;
}

///<summary>���������� ��� ���� (n_string).</summary>
NodeType NodeOfString::getNodeType()
{
    return n_string;
}