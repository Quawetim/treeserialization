#include "Serializator.h"

Serializator::Serializator() {};

Serializator::~Serializator() {};

///<summary>��������� ����� ���� �� ��� ������.</summary>
///<param name='number'>����� ����, ������� ����.</param>
///<param name='nodes'>������ �����, � ������� ����.</param>
///<param name='log'>���-����.</param>
///<returns>���� ��� NULL, ���� �� �������.<returns>
NodeOfBase* Serializator::FindNodeByNumber(int number, std::vector<NodeOfBase*> nodes, std::ofstream &log)
{
    if (nodes.size() > 0)
    {
        for (size_t i = 0; i < nodes.size(); i++)
        {
            if (nodes[i]->getNodeNumber() == number)
            {
                return nodes[i];
            }
        }

        std::cout << "<Error><Serializator::FindNodeByNumber> Can't find node #" << number << "." << std::endl << std::endl;
        log << "<Error><Serializator::FindNodeByNumber> Can't find node #" << number << "." << std::endl << std::endl;
    }
    else
    {
        std::cout << "<Error><Serializator::FindNodeByNumber> List of nodes is empty." << std::endl << std::endl;
        log << "<Error><Serializator::FindNodeByNumber> List of nodes is empty." << std::endl << std::endl;
    }

    return NULL;
}

///<summary>��������� ������������ ������ (����� � ����).
///<para>������ �����:</para>
///<para>type	data	number_of_childs	[numbers]</para>
///<para>type - ��� �������� �����: i - �����, f - ������������, s - ������;</para>
///<para>data - �������� ��������;</para>
///<para>number_of_childs - ����� ����� (��������) ��� 0, ���� �� ���.</para>
///<para>[numbers] - ������ �����, ������� �������� ������.</para>
///</summary>
///<param name='tree'>��������� �� ������.</param>
///<param name='filepath'>����, � ������� ����� �������� ��������������� ������.</param>
///<param name='log'>���-����.</param>
///<returns>���������� ���������� ������������.</returns>
bool Serializator::Serialize(Tree *tree, std::string filepath, std::ofstream &log)
{
    if (tree->getRoot() == NULL)
    {
        std::cout << "<Error><Serializator::Serialize> Tree root is NULL." << std::endl;
        log << "<Error><Serializator::Serialize> Tree root is NULL." << std::endl;
        return false;
    }

    if (_debug)
    {
        std::cout << "<Info><Serializator::Serialize> Serialization starts." << std::endl;
    }

    log << "<Info><Serializator::Serialize> Serialization starts." << std::endl;

    NodeOfBase *tmp_node;
    std::queue<NodeOfBase*> q;
    char type = 'u';
    std::vector<int> childs_numbs;
    std::ofstream fout;
  
    fout.open(filepath);

    /* ����� ������ � ������ � ����� ���������� � ������ ���� � ����. */
    q.push(tree->getRoot());

    while (!q.empty())
    {
        tmp_node = q.front();
        q.pop();

        for (size_t i = 0; i < tmp_node->ListOfChildrens.size(); i++)
        {
            q.push(tmp_node->ListOfChildrens[i]);
            childs_numbs.push_back(tmp_node->ListOfChildrens[i]->getNodeNumber());
        }

        switch (tmp_node->getNodeType())
        {
        case n_int:
        {
            type = 'i';
            break;
        }
        case n_float:
        {
            type = 'f';
            break;
        }
        case n_string:
        {
            type = 's';
            break;
        }
        default:
        {
            std::cout << "<Error><Serializator::Serialize> Unknown NodeType. Can't serialize." << std::endl << std::endl;
            log << "<Error><Serializator::Serialize> Unknown NodeType. Can't serialize." << std::endl << std::endl;
            
            fout.close();
            return false;
        }
        }

        fout << type << "\t" << tmp_node->getDataAsString() << "\t" << childs_numbs.size();

        for (size_t i = 0; i < childs_numbs.size(); i++)
        {
            fout << "\t" << childs_numbs[i];
        }

        if (!q.empty()) fout << std::endl;

        childs_numbs.clear();
    }

    fout.close();

    std::cout << "Serialization complete succesfully. Your tree in file \"" << filepath << "\"." << std::endl << std::endl;
    log << "<Info><Serializator::Serialize> Serialization complete succesfully. Your tree in file \"" << filepath << "\"." << std::endl << std::endl;

    return true;
}

///<summary>��������� �������������� ������ (������ �� �����).
///<para>������ �����:</para>
///<para>type	data	number_of_childs	[numbers]</para>
///<para>type - ��� �������� �����: i - �����, f - ������������, s - ������;</para>
///<para>data - �������� ��������;</para>
///<para>number_of_childs - ����� ����� (��������) ��� 0, ���� �� ���.</para>
///<para>[numbers] - ������ �����, ������� �������� ������.</para>
///</summary>
///<param name='tree'>����� ����� ������ ����� ��������������.</param>
///<param name='filepath'>����, � ������� ��������� ����������������� ������.</param>
///<param name='log'>���-����.</param>
///<returns>���������� ���������� ��������������, ���� ������.</returns>
bool Serializator::Deserialize(Tree *tree, std::string filepath, std::ofstream &log)
{
    int ch_count, ch_numb;
    char type;
    NodeOfBase* tmp_node;    
    std::string data;
    std::ifstream fin;  
    std::queue<int> q;
    std::vector<NodeOfBase*> nodes;

    fin.open(filepath);

    if (!fin)
    {
        std::cout << "<Error><Serializator::Deserialise> File \"" << filepath << "\" not found." << std::endl << std::endl;
        log << "<Error><Serializator::Deserialise> File \"" << filepath << "\" not found." << std::endl << std::endl;
        return false;
    }

    /* ���� ���� ����. */
    if (fin.peek() == std::ifstream::traits_type::eof())
    {
        std::cout << "<Error><Serializator::Deserialise> File \"" << filepath << "\" is empty." << std::endl << std::endl;
        log << "<Error><Serializator::Deserialise> File \"" << filepath << "\" is empty." << std::endl << std::endl;

        fin.close();
        return false;
    }

    log << "<Info><Serializator::Deserialize> Deserialization starts. File: \"" << filepath << "\"." << std::endl;   

    /* ������ ������ ���� �� ����� ����� � ������ ����. */
    for (int i = 1; !fin.eof(); i++)
    {
        fin >> type >> data >> ch_count;
        q.push(ch_count);

        for (int j = 0; j < ch_count; j++)
        {
            fin >> ch_numb;

            if (ch_numb < 0)
            {
                std::cout << "<Error><Serializator::Deserialize> Child's number of node #" << i << " is < 0. Or wrong file format." << std::endl << std::endl;
                log << "<Error><Serializator::Deserialize> Child's number of node #" << i << " is < 0. Or wrong file format." << std::endl << std::endl;

                fin.close();
                return false;
            }

            /* ���� ����� ������� ����� ������ ��������, �� ����. */
            if (ch_numb == i)
            {
                std::cout << "<Error><Serializator::Deserialize> Loop detected. Node #" << i << " is child of itself." << std::endl << std::endl;
                log << "<Error><Serializator::Deserialize> Loop detected. Node #" << i << " is child of itself." << std::endl << std::endl;
                
                fin.close();
                return false;
            }

            /* ���� ����� ������� ������ ������ ��������, �� ����� ����. */
            if (ch_numb < i)
            {
                std::cout << "<Error><Serializator::Deserialize> Loop detected. Parent #" << i << ", data = " << data 
                    << ". Child #" << ch_numb << ", data = " << FindNodeByNumber(ch_numb, nodes, log)->getDataAsString() << std::endl << std::endl;
                log << "<Error><Serializator::Deserialize> Loop detected. Parent #" << i << ", data = " << data
                    << ". Child #" << ch_numb << ", data = " << FindNodeByNumber(ch_numb, nodes, log)->getDataAsString() << std::endl << std::endl;
                
                fin.close();
                return false;
            }

            q.push(ch_numb);
        }

        switch (type)
        {
        case 'i':
        {
            tmp_node = new NodeOfInt(i, stoi(data), log);
            break;
        }
        case 'f':
        {
            tmp_node = new NodeOfFloat(i, stof(data), log);
            break;
        }
        case 's':
        {
            tmp_node = new NodeOfString(i, data, log);
            break;
        }
        default:
        {
            std::cout << "<Error><Serializator::Deserialise> Unknown NodeType. Can't deserialize." << std::endl << std::endl;
            log << "<Error><Serializator::Deserialise> Unknown NodeType. Can't deserialize." << std::endl << std::endl;              
            return false;
        }
        }

        nodes.push_back(tmp_node);
    }

    /* � ������ ������� ���� ������� ��� ��������. */
    int parent_numb = nodes[0]->getNodeNumber() - 1;

    while (!q.empty())
    {
        /* �������� �� ������� ����� �������� ���� parent_numb. */
        ch_count = q.front();
        q.pop();

        /* � ������ ������� ��� ��������, ���� ��� ����. */
        for (int i = 0; i < ch_count; i++)
        {
            ch_numb = q.front();
            q.pop();

            tmp_node = FindNodeByNumber(ch_numb, nodes, log);

            if (tmp_node == NULL)
            {
                std::cout << "<Error><Serializator::Deserialize> Deserialization fails." << std::endl << std::endl;
                log << "<Error><Serializator::Deserialize> Deserialization fails." << std::endl << std::endl;
                return false;
            }

            nodes[parent_numb]->ListOfChildrens.push_back(tmp_node);
        }

        parent_numb++;
    }

    tree->setRoot(nodes[0]);

    fin.close();

    nodes.clear();

    std::cout << "Deserialization of file \"" << filepath << "\" complete succesfully." << std::endl << std::endl;
    log << "<Info><Serializator::Deserialize> Deserialization of file \"" << filepath << "\" complete succesfully." << std::endl << std::endl;

    return true;
}