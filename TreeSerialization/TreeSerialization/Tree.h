#include <queue>
#include "NodeOf.h"

///<summary>��������� ������.</summary>
class Tree
{
private:
    NodeOfBase* _root;

    bool DFS(NodeOfBase* node, std::ofstream &log);
    bool BFS(NodeOfBase* node, std::ofstream &log);    

public:   
    Tree();
    ~Tree();

    ///<summary>����� ������ ������.</summary>
    ///<param name='root'>������ ������.</root>
    void setRoot(NodeOfBase* root);

    ///<summary>���������� ������ ������.</summary>
    NodeOfBase* getRoot();

    ///<summary>����� ������ �� �����.
    ///<para>������ ������:</para>
    ///<para>ParentNode (nil | ChildNode1, ChildNode2, ...)</para>
    ///</summary>
    ///<param name='log'>���-����.</param>
    ///<returns>���������� ������ �� �����.</returns>
    bool PrintToScreen(std::ofstream &log);
};
