#include <conio.h>
#include "Serializator.h"

int main(int argc, char* argv[])
{
    bool result;
    std::ofstream log;
    Tree *tree = new Tree();
    Serializator *S = new Serializator();

    switch (argc)
    {
    case 2:
    {
        if (!strcmp(argv[1], "-help"))
        {
            std::cout << "\"TreeSerialization -help\":" << std::endl;
            std::cout << "\t -help - prints this help." << std::endl;

            std::cout << std::endl;

            std::cout << "\"TreeSerialization -t num_of_tests\":" << std::endl;
            std::cout << "\t -t - starts autotests from folder \"tests\", each test in it's own folder (1, 2, 3, 4, ...);" << std::endl;
            std::cout << "\t num_of_tests - number of tests to execute from 1 to num_of_tests. Must be > 0." << std::endl;

            std::cout << std::endl;

            std::cout << "\"TreeSerialization -i input_file\":" << std::endl;
            std::cout << "\t -i - starts deserialization of tree from file input_file and prints it to screen." << std::endl;

            std::cout << std::endl;

            std::cout << "\"TreeSerialization -i input_file -o output_file\":" << std::endl;
            std::cout << "\t -i - starts deserialization of tree from file input_file and prints it to screen;" << std::endl;
            std::cout << "\t -o - after deserialization starts serialization of tree to file output_file." << std::endl;

            std::cout << std::endl;
        }
        else
        {
            std::cout << "Unknown argument " << argv[1] << ". Type \"TreeSerialization -help\" to see help" << std::endl;
        }

        break;
    }
    case 3:
    {
        if (!strcmp(argv[1], "-t"))
        {
            int number_of_tests = atoi(argv[2]);

            if (number_of_tests > 0)
            {
                for (int test = 1; test <= number_of_tests; test++)
                {
                    std::cout << "Test #" << test << std::endl << std::endl;

                    log.open("tests/" + std::to_string(test) + "/log.txt");

                    result = S->Deserialize(tree, "tests/" + std::to_string(test) + "/input.txt", log);

                    if (result)
                    {
                        result = tree->PrintToScreen(log);

                        if (result)
                        {
                            S->Serialize(tree, "tests/" + std::to_string(test) + "/output.txt", log);
                        }
                    }

                    log.close();
                }

                system("pause");
            }
            else
            {
                std::cout << "number_of_test <= 0. Must be > 0." << std::endl;
            }
        }
        else
        {
            if (!strcmp(argv[1], "-i"))
            {
                log.open("log.txt");

                result = S->Deserialize(tree, argv[2], log);

                if (result)
                {
                    tree->PrintToScreen(log);
                }

                log.close();
            }
            else
            {
                std::cout << "Unknown argument #1. Must be -t or -i." << std::endl;
            }
        }

        break;
    }
    case 5:
    {
        if (!strcmp(argv[1], "-i"))
        {
            if (!strcmp(argv[3], "-o"))
            {
                log.open("log.txt");

                result = S->Deserialize(tree, argv[2], log);

                if (result)
                {
                    result = tree->PrintToScreen(log);

                    if (result)
                    {
                        S->Serialize(tree, argv[4], log);
                    }
                }
            }
            else
            {
                std::cout << "Unknown argument #3. Must be -o." << std::endl;
            }
        }
        else
        {
            std::cout << "Unknown argument #1. Must be -i." << std::endl;
        }

        break;
    }
    default:
    {
        std::cout << "Unknown input. Type \"TreeSerialization -help\" to see help." << std::endl;
        break;
    }
    }

    return 0;
}